package com.team3;

import com.team3.utils.Endpoints;


public class Main {

    public static void main(String[] args) {
        
        try {
            //"http://localhost:5000/"
            Endpoints endpoints = new Endpoints();
            endpoints.startServer();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
