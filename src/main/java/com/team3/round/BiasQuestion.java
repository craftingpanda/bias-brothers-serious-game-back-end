package com.team3.round;

public class BiasQuestion {

    private Long id;
    private int points;
    private Bias bias;

    public BiasQuestion(Long id, int points, Bias bias) {
        this.id = id;
        this.points = points;
        this.bias = bias;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public Bias getBias() {
        return bias;
    }

    public void setBias(Bias bias) {
        this.bias = bias;
    }
}
