package com.team3.round;

import java.util.ArrayList;

public class Canvas {

    private Long id;
    private String name;
    private int points;
    private String value1;
    private String value2;
    private String value3;
    private ArrayList<NewsArticle> newsArticles;
    private ArrayList<Metric> metrics;

    public Canvas(Long id, String name, int points, String value1, String value2, String value3, ArrayList<NewsArticle> newsArticles, ArrayList<Metric> metrics) {
        this.id = id;
        this.name = name;
        this.points = points;
        this.value1 = value1;
        this.value2 = value2;
        this.value3 = value3;
        this.newsArticles = newsArticles;
        this.metrics = metrics;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public String getValue1() {
        return value1;
    }

    public void setValue1(String value1) {
        this.value1 = value1;
    }

    public String getValue2() {
        return value2;
    }

    public void setValue2(String value2) {
        this.value2 = value2;
    }

    public String getValue3() {
        return value3;
    }

    public void setValue3(String value3) {
        this.value3 = value3;
    }

    public ArrayList<NewsArticle> getNewsArticles() {
        return newsArticles;
    }

    public void setNewsArticleCollection(ArrayList<NewsArticle> newsArticles) {
        this.newsArticles = newsArticles;
    }

    public ArrayList<Metric> getMetrics() {
        return metrics;
    }

    public void setMetrics(ArrayList<Metric> metrics) {
        this.metrics = metrics;
    }
}
