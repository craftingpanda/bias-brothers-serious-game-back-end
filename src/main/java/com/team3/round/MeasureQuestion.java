package com.team3.round;

public class MeasureQuestion {

    private Long id;
    private Measure measure;
    private int points;

    public MeasureQuestion(Long id, Measure measure, int points) {
        this.id = id;
        this.measure = measure;
        this.points = points;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Measure getMeasure() {
        return measure;
    }

    public void setMeasure(Measure measure) {
        this.measure = measure;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }
}
