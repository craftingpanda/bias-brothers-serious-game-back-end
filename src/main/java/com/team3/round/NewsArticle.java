package com.team3.round;

public class NewsArticle {

    private Long id;
    private String title;
    private String message;
    private String source;
    private boolean popUp;

    public NewsArticle(Long id, String title, String message, String source, boolean popUp) {
        this.id = id;
        this.title = title;
        this.message = message;
        this.source = source;
        this.popUp = popUp;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public boolean isPopUp() {
        return popUp;
    }

    public void setPopUp(boolean popUp) {
        this.popUp = popUp;
    }
}
