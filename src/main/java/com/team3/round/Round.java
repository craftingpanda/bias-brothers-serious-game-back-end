package com.team3.round;

import java.util.ArrayList;

public class Round {

    private Long id;
    private String roundNumber;
    private String title;
    private Timer timer;
    private Scenario scenario;
    private ArrayList<Canvas> canvases;

    public Round(Long id, String roundNumber, String title, Timer timer, Scenario scenario, ArrayList<Canvas> canvases) {
        this.id = id;
        this.roundNumber = roundNumber;
        this.title = title;
        this.timer = timer;
        this.scenario = scenario;
        this.canvases = canvases;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRoundNumber() {
        return roundNumber;
    }

    public void setRoundNumber(String roundNumber) {
        this.roundNumber = roundNumber;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Timer getTimer() {
        return timer;
    }

    public void setTimer(Timer timer) {
        this.timer = timer;
    }

    public Scenario getScenario() {
        return scenario;
    }

    public void setScenario(Scenario scenario) {
        this.scenario = scenario;
    }

    public ArrayList<Canvas> getCanvases() {
        return canvases;
    }

    public void setCanvases(ArrayList<Canvas> canvases) {
        this.canvases = canvases;
    }
}
