package com.team3.round;

import java.util.ArrayList;

public class Scenario {

    private Long id;
    private String title;
    private String text;
    private ArrayList<Canvas> canvasses;
    private ArrayList<BiasQuestion> biasQuestions;
    private ArrayList<MeasureQuestion> measureQuestions;

    public Scenario(Long id, String title, String text, ArrayList<Canvas> canvasses, ArrayList<BiasQuestion> biasQuestions, ArrayList<MeasureQuestion> measureQuestions) {
        this.id = id;
        this.title = title;
        this.text = text;
        this.canvasses = canvasses;
        this.biasQuestions = biasQuestions;
        this.measureQuestions = measureQuestions;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public ArrayList<Canvas> getCanvasses() {
        return canvasses;
    }

    public void setCanvasses(ArrayList<Canvas> canvasses) {
        this.canvasses = canvasses;
    }

    public ArrayList<BiasQuestion> getBiasQuestions() {
        return biasQuestions;
    }

    public void setBiasQuestions(ArrayList<BiasQuestion> biasQuestions) {
        this.biasQuestions = biasQuestions;
    }

    public ArrayList<MeasureQuestion> getMeasureQuestions() {
        return measureQuestions;
    }

    public void setMeasureQuestions(ArrayList<MeasureQuestion> measureQuestions) {
        this.measureQuestions = measureQuestions;
    }
}

