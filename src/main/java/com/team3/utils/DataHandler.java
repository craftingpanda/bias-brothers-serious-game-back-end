package com.team3.utils;

import com.team3.round.Bias;
import com.team3.round.Round;
import com.google.gson.Gson;

import java.io.*;
import java.util.ArrayList;

public class DataHandler {

    private ArrayList<Round> setupRoundData() {
        try {
            Gson gson = new Gson();

            InputStream round1JSON = getClass().getResourceAsStream("/data/round1.json");
            BufferedReader reader1 = new BufferedReader(new InputStreamReader(round1JSON));

            InputStream round2JSON = getClass().getResourceAsStream("/data/round2.json");
            BufferedReader reader2 = new BufferedReader(new InputStreamReader(round2JSON));

            InputStream round3JSON = getClass().getResourceAsStream("/data/round3.json");
            BufferedReader reader3 = new BufferedReader(new InputStreamReader(round3JSON));

            InputStream round4JSON = getClass().getResourceAsStream("/data/round4.json");
            BufferedReader reader4 = new BufferedReader(new InputStreamReader(round4JSON));

            InputStream round5JSON = getClass().getResourceAsStream("/data/round5.json");
            BufferedReader reader5 = new BufferedReader(new InputStreamReader(round5JSON));

            InputStream round6JSON = getClass().getResourceAsStream("/data/round6.json");
            BufferedReader reader6 = new BufferedReader(new InputStreamReader(round6JSON));

            Round round1 = gson.fromJson(reader1, Round.class);
            Round round2 = gson.fromJson(reader2, Round.class);
            Round round3 = gson.fromJson(reader3, Round.class);
            Round round4 = gson.fromJson(reader4, Round.class);
            Round round5 = gson.fromJson(reader5, Round.class);
            Round round6 = gson.fromJson(reader6, Round.class);

            reader1.close();
            reader2.close();
            reader3.close();
            reader4.close();
            reader5.close();
            reader6.close();

            ArrayList<Round> rounds = new ArrayList<>();
            rounds.add(round1);
            rounds.add(round2);
            rounds.add(round3);
            rounds.add(round4);
            rounds.add(round5);
            rounds.add(round6);

            return rounds;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private ArrayList<Bias> setupBiasListData() {
        try {
            Gson gson = new Gson();

            InputStream inputBiasList = getClass().getResourceAsStream("/data/BiasList.json");
            BufferedReader biasListReader = new BufferedReader(new InputStreamReader(inputBiasList));

            ArrayList<Bias> biasList = gson.fromJson(biasListReader, ArrayList.class);

            biasListReader.close();

            return biasList;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public ArrayList<Bias> getBiasListData()  {
        return setupBiasListData();
    }

    public ArrayList<Round> getRoundsData() {
        return setupRoundData();
    }
}


