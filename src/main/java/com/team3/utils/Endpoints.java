package com.team3.utils;

import com.google.gson.Gson;
import com.team3.round.Bias;
import com.team3.round.Round;
import spark.Spark;

import java.util.ArrayList;

import static spark.Spark.*;

public class Endpoints {

    private final MeasureQuestionHandler measureQuestionHandler = new MeasureQuestionHandler();
    private final DataHandler dataHandler = new DataHandler();

    public void startServer() {
        //////////////////////////////////////////
        port(8080); // port amazon for endpoints
        /////////////////////////////////////////

        // Allow CORS//////////////////////////////////////////////////////////////////////////////
        Spark.options("/*", (request, response) -> {
            String accessControlRequestHeaders = request.headers("Access-Control-Request-Headers");
            if (accessControlRequestHeaders != null) {
                response.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
            }
            String accessControlRequestMethod = request.headers("Access-Control-Request-Method");
            if (accessControlRequestMethod != null) {
                response.header("Access-Control-Allow-Methods", accessControlRequestMethod);
            }
            return "OK";

        });

        Spark.before((request, response) -> {
            response.header("Access-Control-Allow-Origin", "*");

            get("/", (req, res) -> "Serious Game");
        });
        ////////////////////////////////////////////////////////////////////////////////////////////

        final Gson gson = new Gson();

        get("/round/:number", (req, res) -> {
            int roundNumber = Integer.parseInt(req.params(":number"));
            Round round = dataHandler.getRoundsData().get(roundNumber - 1);

            return gson.toJson(round);
        });

        get("/checkAnswer/:round/:canvas_number/:measure_points", (req, res) -> {
            int canvasNumber = Integer.parseInt(req.params(":canvas_number"));
            int canvasPoints = Integer.parseInt(req.params(":measure_points"));

            int newCanvasNumber = measureQuestionHandler.checkAnswer(canvasNumber, canvasPoints);

            return gson.toJson(newCanvasNumber);
        });

        get("/bias", (req, res) -> {
            ArrayList<Bias> biasList = dataHandler.getBiasListData();

            return gson.toJson(biasList);
        });

        after((req, res) -> {
            res.status(200);
            res.type("application/json");
        });

        notFound((req, res) -> {
            res.type("application/json");
            res.status(404);
            return "{\"message\":\"404 Not Found\"}";
        });
    }
}
